export interface Vector {
    x: number
    y: number
    z: number
}

export interface BoidConstructor {
    worldSize: Vector
    options?: {
        position?: Vector
        velocity?: Vector
        boundaryTurningForce?: number
        visualRange?: number
        protectedRange?: number
        centeringForce?: number
        avoidanceForce?: number
        alignmentForce?: number
        maxSpeed?: number
        minSpeed?: number
        fieldMargin?: number
    }
}

const BOID_DEFAULTS = {
    boundaryTurningForce: 0.2,
    visualRange: 200,
    protectedRange: 40,
    centeringForce: 0.0005,
    avoidanceForce: 0.15,
    alignmentForce : 0.05,
    maxSpeed: 3,
    minSpeed: 1,
    fieldMargin: 100,
}

export default class Boid {
    position: Vector
    velocity: Vector

    readonly worldSize: Vector
    readonly id: string
    readonly boundaryTurningForce: number
    readonly visualRange: number
    readonly protectedRange: number
    readonly centeringForce: number
    readonly avoidanceForce: number
    readonly alignmentForce: number
    readonly maxSpeed: number
    readonly minSpeed: number
    readonly fieldMargin: number

    static readonly defaults = BOID_DEFAULTS

    constructor(props: BoidConstructor) {
        // generate random position for each boid
        this.position = props.options?.position || {
            x: this.randomIntFromInterval(0, props.worldSize.x),
            y: this.randomIntFromInterval(0, props.worldSize.y),
            z: this.randomIntFromInterval(0, props.worldSize.z)
        }
        this.velocity = props.options?.velocity || {
            x: 0,
            y: 0,
            z: 0
        }

        this.worldSize = props.worldSize
        // generate UUID for each boid
        const id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
        this.id = id

        // set default values for the boid
        this.boundaryTurningForce = props.options?.boundaryTurningForce || Boid.defaults.boundaryTurningForce
        this.visualRange = props.options?.visualRange || Boid.defaults.visualRange
        this.protectedRange = props.options?.protectedRange || Boid.defaults.protectedRange
        this.centeringForce = props.options?.centeringForce || Boid.defaults.centeringForce
        this.avoidanceForce = props.options?.avoidanceForce || Boid.defaults.avoidanceForce
        this.alignmentForce = props.options?.alignmentForce || Boid.defaults.alignmentForce
        this.maxSpeed = props.options?.maxSpeed || Boid.defaults.maxSpeed
        this.minSpeed = props.options?.minSpeed || Boid.defaults.minSpeed
        this.fieldMargin = props.options?.fieldMargin || Boid.defaults.fieldMargin
    }

    // update the position of the boid
    public updatePosition = (boids: Boid[]): void => {
        this.applySeparationForce(boids)
        this.applyAlignmentForce(boids)
        this.applyCohesionForce(boids)
        this.applyBoundaryForce()
        this.clampVelocity()

        this.position.x += this.velocity.x
        this.position.y += this.velocity.y
        this.position.z += this.velocity.z
    }


    private distanceTo = (boid: Boid): number => {
        return Math.sqrt((this.position.x - boid.position.x) ** 2 + (this.position.y - boid.position.y) ** 2 + (this.position.z - boid.position.z) ** 2)
    }

    // 1. Separation Force
    // Each boid attempts to avoid running into other boids.
    // If two or more boids get too close to one another (i.e. within one another's protected range),
    // they will steer away from one another to avoid a collision.
    private applySeparationForce = (boids: Boid[]): void => {
        let neighbors_dy = 0
        let neighbors_dx = 0
        let neighbors_dz = 0

        // get the sum of the distances between the current boid and all other boids
        for (const boid of boids) {
            if (this.id !== boid.id) {
                const distance = this.distanceTo(boid)
                if (distance < this.protectedRange) {
                    neighbors_dx += this.position.x - boid.position.x
                    neighbors_dy += this.position.y - boid.position.y
                    neighbors_dz += this.position.z - boid.position.z
                }
            }
        }

        // update the velocity of the current boid
        this.velocity.x += neighbors_dx * this.avoidanceForce
        this.velocity.y += neighbors_dy * this.avoidanceForce
        this.velocity.z += neighbors_dz * this.avoidanceForce
    }

    // 2. Alignment Force
    // Each boid attempts to match the velocity of its neighbors.
    // If a boid sees that its neighbors are moving in a certain direction, it will attempt to move in that direction as well.
    private applyAlignmentForce = (boids: Boid[]): void => {
        let neighbors_dx = 0
        let neighbors_dy = 0
        let neighbors_dz = 0

        let neighbors_count = 0

        // get the sum of the velocities of the current boid and all other boids
        for (const boid of boids) {
            if (this.id !== boid.id) {
                const distance = this.distanceTo(boid)
                if (distance < this.visualRange) {
                    neighbors_dx += boid.velocity.x
                    neighbors_dy += boid.velocity.y
                    neighbors_dz += boid.velocity.z
                    neighbors_count += 1
                }
            }
        }

        // if neighbors are found, calculate the average velocity
        if (neighbors_count > 0) {
            neighbors_dx /= neighbors_count
            neighbors_dy /= neighbors_count
            neighbors_dz /= neighbors_count
        }

        // update the velocity of the current boid
        this.velocity.x += neighbors_dx * this.alignmentForce
        this.velocity.y += neighbors_dy * this.alignmentForce
        this.velocity.z += neighbors_dz * this.alignmentForce
    }


    // 3. Cohesion Force
    // Each boid attempts to move towards the average position of its neighbors.
    // If a boid sees that its neighbors are clustered in a certain area, it will attempt to move towards that area.
    private applyCohesionForce = (boids: Boid[]): void => {
        let dx_avg = 0
        let dy_avg = 0
        let dz_avg = 0
        let neighbors_count = 0

        //  If the distance to a particular boid is less than the visible range
        for (const boid of boids) {
            if (this.id !== boid.id) {
                const distance = this.distanceTo(boid)
                if (distance < this.visualRange) {
                    dx_avg += boid.position.x
                    dy_avg += boid.position.y
                    dz_avg += boid.position.z
                    neighbors_count += 1
                }
            }
        }

        // if neighbors are found, calculate the average position
        if (neighbors_count > 0) {
            dx_avg /= neighbors_count
            dy_avg /= neighbors_count
            dz_avg /= neighbors_count
        }

        // update the velocity of the current boid
        this.velocity.x += (dx_avg - this.position.x) * this.centeringForce
        this.velocity.y += (dy_avg - this.position.y) * this.centeringForce
        this.velocity.z += (dz_avg - this.position.z) * this.centeringForce
    }


    // 4. Boundary Force
    // Each boid attempts to stay within a certain boundary.
    // If a boid sees that it is moving outside of a certain boundary, it will attempt to move back inside that boundary.
    private applyBoundaryForce = (): void => {
        if (this.position.x < this.fieldMargin) {
            this.velocity.x += this.boundaryTurningForce
        }
        if (this.position.x > this.worldSize.x - this.fieldMargin) {
            this.velocity.x -= this.boundaryTurningForce
        }
        if (this.position.y < this.fieldMargin) {
            this.velocity.y += this.boundaryTurningForce
        }
        if (this.position.y > this.worldSize.y - this.fieldMargin) {
            this.velocity.y -= this.boundaryTurningForce
        }
        if (this.position.z < this.fieldMargin) {
            this.velocity.z += this.boundaryTurningForce
        }
        if (this.position.z > this.worldSize.z - this.fieldMargin) {
            this.velocity.z -= this.boundaryTurningForce
        }
    }

    // 5. clamp velocity
    // constrain the velocity of the boid to a certain range
    private clampVelocity = (): void => {
        const speed = Math.sqrt(this.velocity.x ** 2 + this.velocity.y ** 2 + this.velocity.z ** 2)
        if (speed > this.maxSpeed) {
            this.velocity.x = (this.velocity.x / speed) * this.maxSpeed
            this.velocity.y = (this.velocity.y / speed) * this.maxSpeed
            this.velocity.z = (this.velocity.z / speed) * this.maxSpeed
        } else if (speed < this.minSpeed) {
            this.velocity.x = (this.velocity.x / speed) * this.minSpeed
            this.velocity.y = (this.velocity.y / speed) * this.minSpeed
            this.velocity.z = (this.velocity.z / speed) * this.minSpeed
        }
    }

    private randomIntFromInterval = (min: number, max: number) => {
        return Math.floor(Math.random() * (max - min + 1) + min)
    }
}

