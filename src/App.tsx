import React from 'react'
import { BoidSimulation } from './components/Simulation'

export const App = () => {
    return (
        <BoidSimulation
            boidCount={200}
            worldSize={{
                x: window.innerWidth,
                y: window.innerHeight,
                z: window.innerHeight
            }}
        />
    )
}
