import React from 'react'
import * as THREE from 'three'
import Stats from 'stats.js'
import { OrbitControls } from 'three/examples/jsm/Addons.js'

import type Boid from '../Boid'

export interface AnimationProps {
    boids: Boid[],
    renderSize: {x: number, y: number}
    drawVisionBoundaries?: boolean
    drawWorldBoundaries?: boolean
    showFrameRate?: boolean
}

export const AnimationController = ({
    boids,
    renderSize,
    drawVisionBoundaries = false,
    drawWorldBoundaries = false,
    showFrameRate = false
}: AnimationProps) => {
    // used by three to inject a canvas element
    const containerRef = React.useRef<HTMLDivElement>(null)

    // called when the component is unmounted
    const disposeOfScene = (scene, renderer) => {
        // Traverse the whole scene
        scene.traverse((child) => {
            // Test if it's a mesh
            if (child instanceof THREE.Mesh) {
                child.geometry.dispose()
                // Loop through the material properties
                for (const key in child.material) {
                    if (typeof child.material[key] === 'object') {
                        const value = child.material[key]
                        // Test if there is a dispose function
                        if (value && typeof value.dispose === 'function') {
                            value.dispose()
                        }
                    }
                }
            }
        })
        renderer.dispose()
    }


    // called when the component is mounted
    React.useEffect(() => {
        // fps counter
        const stats = new Stats()
        stats.showPanel( 0 )
        stats.dom.style.right = '0px'
        stats.dom.style.left = 'unset'
        if (showFrameRate) {
            stats.dom.style.display = 'block'
        } else {
            stats.dom.style.display = 'none'
        }
        containerRef.current && containerRef.current.appendChild( stats.dom )

        const scene = new THREE.Scene()
        scene.background = new THREE.Color( 0xf0f0f0 )

        const renderer = new THREE.WebGLRenderer()
        renderer.setSize( renderSize.x, renderSize.y)
        renderer.setAnimationLoop( animate )
        containerRef.current && containerRef.current.appendChild( renderer.domElement )

        const camera = new THREE.PerspectiveCamera(75, renderSize.x / renderSize.y, 0.1, 10000)
        const controls = new OrbitControls( camera, renderer.domElement );
        camera.position.z = renderSize.y

        // optionally render boid vision radius as circles
        const circleGeometry = new THREE.CircleGeometry( boids[0].visualRange, 32 )
        const circleMaterial = new THREE.MeshBasicMaterial( { color: 0x8FFBFF } )
        const circleMeshes = drawVisionBoundaries && boids.map(boid => {
            const circle = new THREE.Mesh( circleGeometry, circleMaterial )
            circle.position.set(boid.position.x, boid.position.y, 0)
            // add opacity to circle
            circle.material.transparent = true
            circle.material.opacity = 0.2
            scene.add( circle )
            return circle
        }) || undefined

        // optionally render boid exclusion range as circles
        const exclusionGeometry = new THREE.CircleGeometry( boids[0].protectedRange, 32 )
        const exclusionMaterial = new THREE.MeshBasicMaterial( { color: 0xFFDE59 } )
        const exclusionMeshes = drawVisionBoundaries && boids.map(boid => {
            const circle = new THREE.Mesh( exclusionGeometry, exclusionMaterial )
            circle.position.set(boid.position.x, boid.position.y, 0)
            // add opacity to circle
            circle.material.transparent = true
            circle.material.opacity = 0.2
            scene.add( circle )
            return circle
        }) || undefined

        // optionally render the world boundaries as lines
        if (drawWorldBoundaries) {
            const worldBoundaries = [
                new THREE.Vector2(-boids[0].worldSize.x / 2, -boids[0].worldSize.y / 2),
                new THREE.Vector2(-boids[0].worldSize.x / 2, boids[0].worldSize.y / 2),
                new THREE.Vector2(boids[0].worldSize.x / 2, boids[0].worldSize.y / 2),
                new THREE.Vector2(boids[0].worldSize.x / 2, -boids[0].worldSize.y / 2),
                new THREE.Vector2(-boids[0].worldSize.x / 2, -boids[0].worldSize.y / 2),
            ]
            const lineMaterial = new THREE.LineBasicMaterial({ color: 0x0000ff })
            const lineGeometry = new THREE.BufferGeometry().setFromPoints(worldBoundaries)
            const line = new THREE.Line( lineGeometry, lineMaterial )
            scene.add( line )
        }

        // render boids as cones
        const boidGeometry = new THREE.ConeGeometry(10, 30)
        const boidMaterial = new THREE.MeshLambertMaterial({color: 0xff0000})
        const boidMeshes = boids.map(boid => {
            const boidCones = new THREE.Mesh( boidGeometry, boidMaterial )
            boidCones.position.set(boid.position.x, boid.position.y, boid.position.z)
            scene.add( boidCones )
            return boidCones
        })

        function animate () {
            stats.begin()

            // calculate new positions for each boid
            boids.forEach(boid => {
                boid.updatePosition(boids)
            })


            // update mesh positions
            boidMeshes.forEach((mesh, i) => {
                const boid = boids[i]
                // set position of the mesh to the position of the boid
                mesh.position.set(
                    boid.position.x - boid.worldSize.x / 2,
                    boid.position.y - boid.worldSize.y / 2,
                    boid.position.z - boid.worldSize.z / 2
                )
                // set the rotation of the mesh to the direction of the boid
                mesh.rotation.z = Math.atan2(boid.velocity.y, boid.velocity.x) - Math.PI / 2
            })

            // update circle positions
            if (drawVisionBoundaries) {
                circleMeshes?.forEach((mesh, i) => {
                    const boid = boids[i]
                    mesh.position.set(boid.position.x - boid.worldSize.x / 2, boid.position.y - boid.worldSize.y / 2, 0)
                })
                exclusionMeshes?.forEach((mesh, i) => {
                    const boid = boids[i]
                    mesh.position.set(boid.position.x - boid.worldSize.x / 2, boid.position.y - boid.worldSize.y / 2, 0)
                })
            }

            renderer.render( scene, camera )

            stats.end()
        }

        return () => {
            disposeOfScene(scene, renderer)
        }

    }, [])

    return (
        <div
            ref={containerRef}
        ></div>
    )

}
