import React from 'react'
import styled from 'styled-components'
import useLocalStorage from 'use-local-storage'

const CollapsibleContainer = styled.div`
    display: flex;
    flex-direction: column;
    position: fixed;
    top: 0;
    left: 0;
    background-color: white;
    padding: 10px;
    background-color: unset;
`

const ControlPaneContainer = styled.div<{ $isOpen: boolean }>`
    display: ${({$isOpen}) => $isOpen ? 'block' : 'none'};
    background-color: white;
    padding: 10px;
    border: 1px solid black;
    background-color: unset;
`

export interface ControlPaneProps {
    n: number
    onChange: (props: ControlPaneProps) => void
    onReset: () => void
    boundaryTurningForce: number
    visualRange: number
    protectedRange: number
    centeringForce: number
    avoidanceForce: number
    alignmentForce: number
    maxSpeed: number
    minSpeed: number
    fieldMargin: number
    drawVisionMeshes: boolean
    drawWorldBoundary: boolean
    showFrameRate: boolean
}

export const ControlPane = (props: ControlPaneProps) => {
    const [isPaneOpen, setPaneOpen] = useLocalStorage('isPaneOpen', true)

    return (
        <CollapsibleContainer>
            <button
                onClick={() => setPaneOpen(!isPaneOpen)}
            >
                {isPaneOpen ? 'Minimize' : 'Options'}
            </button>
            <ControlPaneContainer $isOpen={isPaneOpen}>
                <label htmlFor='n'>Number of boids:</label>
                <input
                    name='n'
                    type="number"
                    min="0"
                    max="1000"
                    value={props.n}
                    onChange={(e) => props.onChange({...props, n: parseInt(e.target.value)})}
                />
                <br />
                <label htmlFor='turning'>Boundary Turning Force:</label>
                <input
                    name='turning'
                    type="number"
                    min="0"
                    max="1"
                    step="0.01"
                    value={props.boundaryTurningForce}
                    onChange={(e) => props.onChange({...props, boundaryTurningForce: parseFloat(e.target.value)})}
                />
                <br />
                <label htmlFor='visualRange'>Visual Range:</label>
                <input
                    name='visualRange'
                    type="number"
                    min="0"
                    max="1000"
                    value={props.visualRange}
                    onChange={(e) => props.onChange({...props, visualRange: parseInt(e.target.value)})}
                />
                <br />
                <label htmlFor='protectedRange'>Protected Range:</label>
                <input
                    name='protectedRange'
                    type="number"
                    min="0"
                    max="1000"
                    value={props.protectedRange}
                    onChange={(e) => props.onChange({...props, protectedRange: parseInt(e.target.value)})}
                />
                <br />
                <label htmlFor='centeringForce'>Centering Force:</label>
                <input
                    name='centeringForce'
                    type="number"
                    min="0"
                    max="1"
                    step="0.01"
                    value={props.centeringForce}
                    onChange={(e) => props.onChange({...props, centeringForce: parseFloat(e.target.value)})}
                />
                <br />
                <label htmlFor='avoidanceForce'>Avoidance Force:</label>
                <input
                    name='avoidanceForce'
                    type="number"
                    min="0"
                    max="1"
                    step="0.01"
                    value={props.avoidanceForce}
                    onChange={(e) => props.onChange({...props, avoidanceForce: parseFloat(e.target.value)})}
                />
                <br />
                <label htmlFor='alignmentForce'>Alignment Force:</label>
                <input
                    name='alignmentForce'
                    type="number"
                    min="0"
                    max="1"
                    step="0.01"
                    value={props.alignmentForce}
                    onChange={(e) => props.onChange({...props, alignmentForce: parseFloat(e.target.value)})}
                />
                <br />
                <label htmlFor='maxSpeed'>Max Speed:</label>
                <input
                    name='maxSpeed'
                    type="number"
                    min="0"
                    max="10"
                    step="0.1"
                    value={props.maxSpeed}
                    onChange={(e) => props.onChange({...props, maxSpeed: parseFloat(e.target.value)})}
                />
                <br />
                <label htmlFor='minSpeed'>Min Speed:</label>
                <input
                    name='minSpeed'
                    type="number"
                    min="0"
                    max="10"
                    step="0.1"
                    value={props.minSpeed}
                    onChange={(e) => props.onChange({...props, minSpeed: parseFloat(e.target.value)})}
                />
                <br />
                <label htmlFor='fieldMargin'>Draw Vision Meshes:</label>
                <input
                    name='drawVisionMeshes'
                    type="checkbox"
                    checked={props.drawVisionMeshes}
                    onChange={(e) => props.onChange({...props, drawVisionMeshes: e.target.checked})}
                />
                <br />
                <label htmlFor='fieldMargin'>Draw World Boundary:</label>
                <input
                    name='drawWorldBoundary'
                    type="checkbox"
                    checked={props.drawWorldBoundary}
                    onChange={(e) => props.onChange({...props, drawWorldBoundary: e.target.checked})}
                />
                <br />

                <label htmlFor='fieldMargin'>Show Frame Rate:</label>
                <input
                    name='showFrameRate'
                    type="checkbox"
                    checked={props.showFrameRate}
                    onChange={(e) => props.onChange({...props, showFrameRate: e.target.checked})}
                />

                <hr/>

                <button
                    onClick={() => location.reload()}
                >Apply</button>

                <button onClick={props.onReset}>Reset</button>

            </ControlPaneContainer>
        </CollapsibleContainer>
    )
}