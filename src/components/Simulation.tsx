import React, { useEffect } from 'react'
import useLocalStorage from 'use-local-storage'
import { AnimationController } from './AnimationController'
import { ControlPane, type ControlPaneProps } from './ControlPane'
import Boid from '../Boid'


export interface SimulationProps {
    boidCount: number
    worldSize: {x: number, y: number, z: number}
}

export const BoidSimulation = ({
    boidCount,
    worldSize
}: SimulationProps) => {
    const [userBoidOptions, setUserBoidOptions] = useLocalStorage('boidOptions', {
        n: boidCount,
        drawVisionMeshes: false,
        drawWorldBoundary: false,
        showFrameRate: false,
        ...Boid.defaults
    })

    const [boids, setBoids] = React.useState<Boid[] | undefined>(undefined)

    const handleControlChange = (props: ControlPaneProps) => {
        setUserBoidOptions(props)
    }

    const resetControls = () => {
        window.localStorage.removeItem('boidOptions')
        window.location.reload()
    }

    useEffect(() => {
        setBoids(Array.from({length: userBoidOptions.n}, () => new Boid({
            worldSize,
            options: userBoidOptions
        })))

        return () => {
            setBoids(undefined)
        }
    }, [])

    return (
        <>
            <ControlPane
                onChange={handleControlChange}
                onReset={resetControls}
                {...userBoidOptions}
            />
            {boids && <AnimationController
                boids={boids}
                renderSize={worldSize}
                drawVisionBoundaries={userBoidOptions.drawVisionMeshes}
                drawWorldBoundaries={userBoidOptions.drawWorldBoundary}
                showFrameRate={userBoidOptions.showFrameRate}
            />}
        </>
    )
}